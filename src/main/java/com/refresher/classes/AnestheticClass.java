package main.java.com.refresher.classes;

import java.util.ArrayList;
import java.util.List;

import main.java.com.refresher.Course;

public class AnestheticClass extends Class {
	
	ArrayList<Course> allowedCourses = new ArrayList<Course>();
	
	@Override
	protected List<Course> getAllowedCourses() {
		allowedCourses.add(Course.DENTISTRY);
		allowedCourses.add(Course.NURSING);
		return allowedCourses;
	}
}
