package main.java.com.refresher.classes;

import java.util.ArrayList;
import java.util.List;

import main.java.com.refresher.Course;
import main.java.com.refresher.students.IStudent;
import main.java.com.refresher.students.Student;

public abstract class Class {

	private final List<IStudent> students = new ArrayList<>();
	
	protected abstract List<Course> getAllowedCourses();
	
	List<String> studentsEnrolled = new ArrayList<String>();
	
	protected boolean isCourseAllowed(IStudent student) {
		Boolean result = this.getAllowedCourses().contains(student.getCourse())?(result = true):(result = false);
		return result;
	}
	
	public void addStudent(IStudent student) {
		if(isCourseAllowed(student)) {
			students.add(student);
		}else {
			System.out.println("Student's course is not allowed in the class.");
		}
	}
	
	public List<String> getStudentNames() {
		for(int x = 0; x<students.size(); x++) {
			Student student = (Student) students.get(x);
			studentsEnrolled.add(student.getName());
		}
		return studentsEnrolled;
	}
}