package main.java.com.refresher.students;

import main.java.com.refresher.Course;

public class ITStudent extends Student {
	
	private static final String Motto = "Eat, Sleep, Code";

	public ITStudent(long studentNumber, String name, String address, int age) {
		super(studentNumber, name, address, age);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Course getCourse() {
		// TODO Auto-generated method stub
		return Course.IT;
	}

	@Override
	public String getCourseMotto() {
		// TODO Auto-generated method stub
		return Motto;
	}
	
}
