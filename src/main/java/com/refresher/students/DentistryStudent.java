package main.java.com.refresher.students;

import main.java.com.refresher.Course;

public class DentistryStudent extends Student{
	
	private static final String Motto = "Make you smile";

	public DentistryStudent(long studentNumber, String name, String address, int age) {
		super(studentNumber, name, address, age);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Course getCourse() {
		// TODO Auto-generated method stub
		return Course.DENTISTRY;
	}

	@Override
	public String getCourseMotto() {
		return Motto;
	}

}
