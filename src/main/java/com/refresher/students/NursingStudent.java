package main.java.com.refresher.students;

import main.java.com.refresher.Course;

public class NursingStudent extends Student {
	
	private static final String Motto = "We heal you";

	public NursingStudent(long studentNumber, String name, String address, int age) {
		super(studentNumber, name, address, age);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Course getCourse() {
		// TODO Auto-generated method stub
		return Course.NURSING;
	}

	@Override
	public String getCourseMotto() {
		// TODO Auto-generated method stub
		return Motto;
	}

}
