package main.java.com.refresher;

import java.util.ArrayList;
import java.util.List;

import main.java.com.refresher.classes.AnestheticClass;
import main.java.com.refresher.classes.HistoryClass;
import main.java.com.refresher.students.DentistryStudent;
import main.java.com.refresher.students.IStudent;
import main.java.com.refresher.students.ITStudent;
import main.java.com.refresher.students.NursingStudent;

public class School {

	public static void main(String[] args) {
		
		List<IStudent> students = new ArrayList<>();
		students.add(new ITStudent(2023100000, "Carl", "Makati City", 18));
		students.add(new ITStudent(2023100001, "Sean", "Manila City", 20));
		students.add(new ITStudent(2023100002, "JC", "Quezon City", 18));
		students.add(new ITStudent(2023100003, "Justfer", "Muntinlupa City", 18));
		students.add(new DentistryStudent(2023100004, "Daxz", "Caloocan City", 18));
		students.add(new DentistryStudent(2023100005, "Gio", "Taguig City", 20));
		students.add(new DentistryStudent(2023100006, "Daryll", "San Jose Del Monte City", 18));
		students.add(new NursingStudent(2023100007, "Jordan", "Pateros City", 18));
		students.add(new NursingStudent(2023100008, "Alex", "Calamba City", 20));
		students.add(new NursingStudent(2023100009, "Ricky", "Bacoor City", 18));
		
		AnestheticClass anestheticClass = new AnestheticClass();
		HistoryClass historyClass = new HistoryClass();
		
		addStudentList(students, anestheticClass);
		addStudentList(students, historyClass);
		System.out.println("");
		System.out.println("***********************");
		System.out.println("This is the list of students enrolled in Anesthetic Class:");
		System.out.println(anestheticClass.getStudentNames());
		System.out.println("");
		System.out.println("***********************");
		System.out.println("This is the list of students enrolled in History Class:");
		System.out.println(historyClass.getStudentNames());
	}
	
	/**
	 * This method will accept a list of IStudent and then will add that students depending to what Object is being passed to the method. 
	 * Current implementation accepts any object BUT will only process an instance of HistoryClass and AnestheticClass.
	 * @param <T> This is a generic type to where we will accept either AnestheticClass() or HistoryClass() OR other classes in the future.
	 * @param studentList This is the ArrayList of students 
	 * @param object This is the object class (AnestheticClass() or HistoryClass() OR other classes in the future)
	 */
	public static <T> void addStudentList(List<IStudent> studentList, T object){
		
		if(object instanceof HistoryClass) {
			for(int x = 0; x<studentList.size(); x++) {
				((HistoryClass) object).addStudent(studentList.get(x));
			}
		}else if(object instanceof AnestheticClass) {
			for(int x = 0; x<studentList.size(); x++) {
				((AnestheticClass) object).addStudent(studentList.get(x));
			}
		}
		
	}
}
